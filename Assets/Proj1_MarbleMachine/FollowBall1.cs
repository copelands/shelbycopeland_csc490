using UnityEngine;
using System.Collections;
using System;

#pragma warning disable 0168

public class FollowBall1 : MonoBehaviour 
{
	public GameObject Marbel1, Marbel2, Marbel3, Marbel4, Marbel5, Marbel6;
	public GameObject Marbel7, Marbel8, Marbel9, Marbel10, OriginalCamera;
	private int index, ballFOV;
	private bool keydownL, keydownR, keydownPlusMinus;
	private GameObject UseMe;
	
	// Use this for initialization
	void Start () 
	{
		keydownL = false;
		keydownR = false;
		keydownPlusMinus = false;
		ballFOV = 10;
	    index = 0;
		UseMe = OriginalCamera;
	}
	
	// Update is called once per frame
	void Update () 
	{
		#region Get which item the camera should be using to look
		if(Input.GetKeyUp (KeyCode.LeftArrow))
			keydownL = false;
		if(Input.GetKeyUp (KeyCode.RightArrow))
			keydownR = false;		
		
		
	    if(Input.GetKeyDown(KeyCode.LeftArrow) && !(keydownL || keydownR))
		{
			keydownL = true;
			
			switch(++index)
			{
				case 1:
					UseMe = Marbel1;
					break;
				case 2:
					UseMe = Marbel2;
					break;
				case 3:
					UseMe = Marbel3;
					break;
				case 4:
					UseMe = Marbel4;
					break;
				case 5:
					UseMe = Marbel5;
					break;
				case 6:
					UseMe = Marbel6;
					break;
				case 7:
					UseMe = Marbel7;
					break;
				case 8:
					UseMe = Marbel8;
					break;
				case 9:
					UseMe = Marbel9;
					break;
				case 10:
					UseMe = Marbel10;
					break;
				default:
					index = 0;
					UseMe = OriginalCamera;
					break;
			}
		}//:End(if -> key)
		
		if(Input.GetKeyDown(KeyCode.RightArrow) && !(keydownL || keydownR))
		{		
			keydownR = true;
			
			switch(--index)
			{
				case 1:
					UseMe = Marbel1;
					break;
				case 2:
					UseMe = Marbel2;
					break;
				case 3:
					UseMe = Marbel3;
					break;
				case 4:
					UseMe = Marbel4;
					break;
				case 5:
					UseMe = Marbel5;
					break;
				case 6:
					UseMe = Marbel6;
					break;
				case 7:
					UseMe = Marbel7;
					break;
				case 8:
					UseMe = Marbel8;
					break;
				case 9:
					UseMe = Marbel9;
					break;
				case 10:
					UseMe = Marbel10;
					break;
				default:
					index = 11;
					UseMe = OriginalCamera;
					break;
			}
		}//:End (if <- key)
		#endregion
		
		#region Zoom in/out FOV for the ball
		if(Input.GetKeyDown(KeyCode.KeypadMinus) && !keydownPlusMinus)
		{
			keydownPlusMinus = true;
			ballFOV++;
		}
		
		if(Input.GetKeyDown(KeyCode.KeypadPlus) && !keydownPlusMinus)
		{
			keydownPlusMinus = true;
			ballFOV--;
			if(ballFOV < 1) ballFOV = 1;
		}
		
		if(Input.GetKeyUp(KeyCode.KeypadMinus) || Input.GetKeyUp(KeyCode.KeypadPlus))
			keydownPlusMinus = false;
		#endregion
		
		try
		{
			if(UseMe.Equals(OriginalCamera))
			{
				transform.LookAt(OriginalCamera.transform.position);
				Camera.current.fieldOfView = 44;
			}
			else
			{
				transform.LookAt(UseMe.transform);
				Camera.current.fieldOfView = ballFOV;
			}//:End (if/else)
		}catch(NullReferenceException nre){}
	}
}
