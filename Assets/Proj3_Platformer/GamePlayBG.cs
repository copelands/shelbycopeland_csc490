using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
[ExecuteInEditMode]
public class GamePlayBG : MonoBehaviour 
{
	#region Vars
	public GameObject scenery;
	private Sprites spriteMgr;
	private float _width, _height;
	private Vector3[] verts;
	private Vector2[] uvs;
	private Texture2D bg;
	private float start =  133,
	              end   = -133,
	              scenerystart = -5,
	              sceneryend   = -273;
	
	private float[] groundedareas = new float[] {112f, -85f};
	private float groundedareaWidth = -40f;
	#endregion
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();	
		spriteMgr.GetSprite (spriteMgr.background, ref bg);
		
		_width = bg.width;
		_height = bg.height;
	}
	
	#region can the mario has standing area?
	public bool CanBeGrounded()
	{
		bool A = (transform.position.x > groundedareas[0] + groundedareaWidth)
			                &&
				 (transform.position.x < groundedareas[0]);
		
		bool B = (transform.position.x > groundedareas[1] + groundedareaWidth)
			                &&
				 (transform.position.x < groundedareas[1]);
		
		return (A || B);
	}
	#endregion
	
	void Update()
	{
		#region make sure we dont overscroll
		if(transform.position.x > start)
		{
			transform.position = new Vector3(start, transform.position.y, transform.position.z);
			scenery.transform.position = new Vector3(scenerystart, scenery.transform.position.y, scenery.transform.position.z);
		}
		if(transform.position.x < end)
		{
			transform.position = new Vector3(end, transform.position.y, transform.position.z);
			scenery.transform.position = new Vector3(sceneryend, scenery.transform.position.y, scenery.transform.position.z);
		}
		#endregion
	}
	
	#region paralized snorelax = PARAALLAXXXX?!?!?
	public void Scroll(Vector3 howmuch)
	{
		rigidbody.velocity = howmuch * -1;
		
		scenery.rigidbody.velocity = howmuch * -1;
	}
	
	public bool CanScroll(int direction)
	{
		switch(direction)
		{
			case 1://:left
				return transform.position.x < start;
			
			case 2:
				return transform.position.x > end;
		}
		
		return false;
	}
	#endregion
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{
		//_width  = Camera.main.pixelWidth;
		//_height = Camera.main.pixelHeight;
		
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Vertexlit"));
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}