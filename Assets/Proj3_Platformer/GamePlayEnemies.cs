using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
[ExecuteInEditMode]
public class GamePlayEnemies : MonoBehaviour 
{
	#region Vars
	public PlayerSprite character;
	public bool runner;
	public bool puncher;
	private Sprites spriteMgr;
	private Texture2D[] JugRun;
	private Texture2D[] JugAtk;
	private Texture2D JugStand;
	private Texture2D status;
	private bool dead;
	private int curframe, direction;
	private float cooldown;
	private const int LEFT = -1, RIGHT = 1;
	private float xmin = -140, xmax = 140, animationTime, hitBox;
	#endregion
	
	void Start () 
	{
		#region prepare
		cooldown = 4;
		direction = RIGHT;
		animationTime = 0;
		curframe = 0;
		spriteMgr = new Sprites();
		JugRun = new Texture2D[]
		{
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1)	
		};
		
		JugAtk = new Texture2D[]
		{
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1)
		};
		
		spriteMgr.GetSprite(spriteMgr.enemyRun1, ref JugRun[0]);
		spriteMgr.GetSprite(spriteMgr.enemyRun2, ref JugRun[1]);
		spriteMgr.GetSprite(spriteMgr.enemyRun3, ref JugRun[2]);
		spriteMgr.GetSprite(spriteMgr.enemyRun4, ref JugRun[3]);
		
		spriteMgr.GetSprite(spriteMgr.enemyAtk1, ref JugAtk[0]);
		spriteMgr.GetSprite(spriteMgr.enemyAtk2, ref JugAtk[1]);
		spriteMgr.GetSprite(spriteMgr.enemyAtk3, ref JugAtk[2]);
		spriteMgr.GetSprite(spriteMgr.enemyAtk4, ref JugAtk[3]);
		spriteMgr.GetSprite(spriteMgr.enemyAtk5, ref JugAtk[4]);
		spriteMgr.GetSprite(spriteMgr.enemyAtk6, ref JugAtk[5]);
		
		spriteMgr.GetSprite(spriteMgr.enemyStand, ref JugStand);
		
		status = JugStand;
		
		UpdateMesh();
		#endregion
	}
	
	#region Damage Checks
	void CheckRunDamage()
	{
		//:playerx = my_x +        my_width * my_scale
		//:playery < -40? -> hit
		//:playery == [-40 -> -35]
		
		hitBox = Mathf.Abs(transform.localScale.x * status.width / 2);
			
		if(Mathf.Abs((character.transform.position.x - transform.position.x)) < hitBox)
			if(character.transform.position.y <= -40)
				character.KillType((runner) ? "bulldozed" : "punch");
			else if(character.transform.position.y + 40 < 5)
			{
				dead = true;
				spriteMgr.GetSprite (spriteMgr.enemyHurt, ref status);
				UpdateMesh ();
			}
	}
	
	void CheckHitDamage()
	{
		hitBox = Mathf.Abs(transform.localScale.x * status.width / 2);
		
		//print ("" + hitBox);
			
		if(Mathf.Abs((character.transform.position.x - transform.position.x)) < hitBox)
			if(character.transform.position.y <= -40)
				character.KillType((runner) ? "bulldozed" : "punch");
			else if(character.transform.position.y + 40 < 5)
			{
				dead = true;
				spriteMgr.GetSprite (spriteMgr.enemyHurt, ref status);
				UpdateMesh ();
			}
	}
	#endregion
	

	void Update () 
	{
		animationTime += Time.deltaTime;
		
		#region Enemy Dead
		if(dead)
		{
			if(animationTime > 2)
			{
				enabled = false;
				transform.position = Vector3.up * 999;
			}
			
			return;
		}	
		#endregion
		
		#region EnemyType = Runner
		if(runner)
		{
			CheckRunDamage();
			if(animationTime > 0.2f)
			{
				animationTime = 0;
				rigidbody.velocity = Vector3.right * direction * 5;
				
				curframe++;
				if(curframe > 3)
					curframe = 0;
				
				status = JugRun[curframe];
				
				transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * direction, 
													transform.localScale.y, 
												transform.localScale.z);
				
				if(transform.position.x > xmax)
					direction = LEFT;
				if(transform.position.x < xmin)
					direction = RIGHT;
				
				UpdateMesh ();
			}
		}//:end runner
		#endregion
		
		#region EnemyType = Puncher
		if(puncher)
		{
			if(animationTime > 0.15f)
			{
				curframe++;
				animationTime = 0;
				
				if(curframe < 7)
				{
					if(status != JugStand)
					{
						status = JugStand;
						UpdateMesh();
					}
				}
				else
				{
					CheckHitDamage();
					status = JugAtk[Mathf.Min((curframe - 7), 5)];
					UpdateMesh();
					if(curframe > 16)
					{
						curframe = 0;
					}
				}
			}
		}//:end puncher
		#endregion
	}//:end update
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{
		float _width  = status.width;
		float _height = status.height;
		
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Diffuse"));
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		Vector3[] verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = status;
	}
}
