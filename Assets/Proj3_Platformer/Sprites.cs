using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;


#pragma warning disable 0168, 0219, 0414

#region RECT
[StructLayout(LayoutKind.Sequential)]
public struct RECT
{
	public int x;
	public int y;
	public int w; 
	public int h; 
	
	public override string ToString ()
	{
		 return "x:" + x + 
			    "  y:" + y +
				"  w:" + w +
				"  h:" + h;
	}
};
#endregion

public class Sprites
{
	#region Public Variables
	public string //:BG, FG
		background = "bg_BGREPEAT",
		bush       = "bg_bush",
		floor      = "bg_floor",
		tree       = "bg_tree",
		platform   = "fg_platform";
	
	public string //:Enemy
		enemyHurt  = "jn_dmg",
		enemyRun1  = "jn_run1",
		enemyRun2  = "jn_run2",
		enemyRun3  = "jn_run3",
		enemyRun4  = "jn_run4",
		enemyStand = "jn_stand",
		enemyAtk1  = "jn_swing1",
		enemyAtk2  = "jn_swing2",
		enemyAtk3  = "jn_swing3",
		enemyAtk4  = "jn_swing4",
		enemyAtk5  = "jn_swing5",
		enemyAtk6  = "jn_swing6";
		
	public string //:Player
		playerHurt   = "mario_die",
		playerCrouch = "mario_down",
		playerJump   = "mario_jump",
		playerRun1   = "mario_run1",
		playerRun2   = "mario_run2",
		playerStand  = "mario_stand";
	#endregion
	
	#region Public Properties
	public Vector3[] Verticies
	{
		get { return verts; }
	}
	
	public Vector2[] UVs
	{
		get { return uvmap; }
	}
	
	public int[] Triangles
	{
		get { return triangles; }
	}
	
	public Vector3[] Norms
	{
		get { return norms; }
	}
	#endregion
	
	#region Private Variables
	private int size = 745;
	private string EXT = ".png";
		
	private IDictionary spriteDictionary;
	public Texture2D spriteSheet;
	
	private Vector3[] verts, norms;
	private Vector2[] uvmap;
	private int[] triangles;
	#endregion
		
	public Sprites()
	{
		#region Parse	
		spriteDictionary = new Dictionary<string, RECT>();
		StreamReader sr;
		
		using(sr = new StreamReader("Sprites/SpriteMap.csm"))
		{
			CSMParser parse = new CSMParser();
			string temp = "";
			
			while (!(temp = sr.ReadLine()).Equals("#END"))
			{
				spriteDictionary.Add(temp, parse.GetRECT(sr.ReadLine()));
			}
			
			sr.Close();
		}
		
		/*
		//#DEBUG
		string name;
		RECT r;
		foreach(DictionaryEntry de in spriteDictionary)
		{
			name = de.Key as string;
		 	r = (RECT)de.Value;
			
			Debug.Log (string.Format ("{0}\n{1}", name, r.ToString()));
		}*/
		
		spriteSheet = new Texture2D(745, 745);
		spriteSheet.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Resources/c.png"));
			
		#endregion
		
		
		#region Verts, UVs, Tris
		uvmap = new Vector2[]
					{
			            new Vector2(1, 1),
						new Vector2(1, 0),
						new Vector2(0, 1),
						new Vector2(0, 0)
					};
		
		norms = new Vector3[]
					{
						Vector3.back,
						Vector3.back,
						Vector3.back,
						Vector3.forward
					};
		
		triangles = new int[] {0,1,2,   2,1,3};
		#endregion
	}
	
	
	public void GetSprite(string name, ref Texture2D texture)
	{
		RECT rect = (RECT)spriteDictionary[name];
		
		//Color[] transparent = new Color[rect.w * rect.h];	
		texture = new Texture2D(rect.w, rect.h);	
		
		//for(int i = 0; i < transparent.Length; i++)
		//	transparent[i] = Color.clear;
		
		//texture.SetPixels (transparent);
		
		//transparent = null;
		//System.GC.Collect();
		
		//Debug.Log (rect.ToString());
		texture = new Texture2D(rect.w, rect.h);
		
		Color c;
		
		for(int i = 0; i < rect.w; i++)
		{
			for(int j = 0; j < rect.h; j++)
			{
				c = spriteSheet.GetPixel (rect.x  + i,  size - rect.y - rect.h + j);
				texture.SetPixel(i, j, c);
			}
		}
		
		texture.Apply();
		
		//byte[] a = texture.EncodeToPNG();
		
		//File.WriteAllBytes(Directory.GetCurrentDirectory() + "/Resources/a.png", a);
	}
	
	
	#region PARSER
	private class CSMParser
	{
		/// <summary>
		///   Gets the RECT for this frame.
		/// </summary>
		/// <param name='s'>
		///   String containing frame information.
		/// </param>
		public RECT GetRECT(string frameInfo)
		{
			try
			{
				RECT frame = new RECT();
				string s = frameInfo.Clone() as string;

				s = s.Substring(s.IndexOf("x") + 2);
				frame.x = Int32.Parse(s.Substring(0, s.IndexOf(',')));

				s = s.Substring(s.IndexOf(':') + 1);
				frame.y = Int32.Parse(s.Substring(0, s.IndexOf(',')));

				s = s.Substring(s.IndexOf(':') + 1);
				frame.w = Int32.Parse(s.Substring(0, s.IndexOf(',')));

				s = s.Substring(s.IndexOf(':') + 1);
				frame.h = Int32.Parse(s);
			
				return frame;
			}
			catch(Exception e)
			{
				Debug.Log ("File Altered.");
				//someone messed with the file. unacceptable
				return new RECT();
			}
		}
	}
	#endregion
}
