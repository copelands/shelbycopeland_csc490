using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class PlayerLives : MonoBehaviour
{
	private int lives = 3;
	private Texture2D lifestatus, alive;
	private Sprites spriteMgr;
	// Use this for initialization
	void Start () 
	{
		#region Like scar said... BEEE PREEEPARREEEDDDDD
		if(PlayerPrefs.HasKey ("Lives"))
			lives = PlayerPrefs.GetInt ("Lives");
		
		spriteMgr = new Sprites();
		
		spriteMgr.GetSprite(spriteMgr.playerJump, ref alive);
		
		lifestatus = new Texture2D(alive.width * lives, alive.height);
		
		Color c;
		for(int i = 0; i < lifestatus.width; i++)
		{
			for(int j = 0; j < lifestatus.height; j++)
			{
				c = alive.GetPixel(i % alive.width, j % alive.height);
				lifestatus.SetPixel(i, j, c);
			}
		}
		
		lifestatus.Apply();
		
		UpdateMesh();
		#endregion
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	#region so much death
	public void Death()
	{
		lives--;	
		if(lives == 0)
		{
			PlayerPrefs.SetInt ("Lives", 3);
			
			Application.LoadLevel ("Start");
			return;
		}
		
		lifestatus = new Texture2D(alive.width * lives, alive.height);
		Color c;
		
		for(int i = 0; i < lifestatus.width; i++)
		{
			for(int j = 0; j < lifestatus.height; j++)
			{
				c = alive.GetPixel(i % alive.width, j % alive.height);
				lifestatus.SetPixel(i, j, c);
			}
		}
		
		lifestatus.Apply();		
		UpdateMesh();		
		Application.LoadLevel("GamePlay");
		PlayerPrefs.SetInt("Lives", lives);
		PlayerPrefs.Save ();
	}
	#endregion
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{
		float _width  = lifestatus.width;
		float _height = lifestatus.height;
		
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Diffuse"));
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		Vector3[] verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = lifestatus;
	}
}
