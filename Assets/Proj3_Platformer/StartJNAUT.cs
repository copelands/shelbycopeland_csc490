using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class StartJNAUT : MonoBehaviour 
{
	#region vars
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Vector2[] uvs;
	private Texture2D[] TheJug;
	private int _width, _height;
	private int curframe, maxframe = 3;
	private float timeWanted = 0.15f, curtime, fullRun = 20f, startTime;
	private int xmin = -148, xmax = 148;
	            //0-3        3
	#endregion
	
	// Use this for initialization
	void Start () 
	{
		#region Prepare
		curtime = 0;
		startTime = Time.time;
		spriteMgr = new Sprites();
		
		TheJug = new Texture2D[]
		{
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1)
		};
		
		spriteMgr.GetSprite (spriteMgr.enemyRun1, ref TheJug[0]);
		spriteMgr.GetSprite (spriteMgr.enemyRun2, ref TheJug[1]);
		spriteMgr.GetSprite (spriteMgr.enemyRun3, ref TheJug[2]);
		spriteMgr.GetSprite (spriteMgr.enemyRun4, ref TheJug[3]);
		
		//TheJug[0].
		
		_width = TheJug[0].width;
		_height = TheJug[0].height;
		UpdateMesh();
		#endregion
	}
	
	void Update()
	{
		#region Run jugger runnnn!!!
		curtime += Time.deltaTime;
		//Debug.Log (curtime + "");
		
		if (curtime >= timeWanted)
		{
			curtime = 0;
			curframe++;
			
			if(curframe > maxframe)
				curframe = 0;
			
			_width = TheJug[curframe].width;
			_height = TheJug[curframe].height;
			transform.position = Vector3.Lerp
								(
									transform.position, 
									new Vector3(xmax + 10, transform.position.y, transform.position.z), 
									(Time.time - startTime) / fullRun 
								);
			
			if(transform.position.x >= xmax)
			{
				transform.position = new Vector3(xmin, transform.position.y, transform.position.z);
				startTime = Time.time;
			}
			
			UpdateMesh ();
		}	
		#endregion
	}
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Vertexlit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = TheJug[curframe];
	}
}
