using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
[ExecuteInEditMode]
public class PlayerSprite : MonoBehaviour 
{
	#region vars
	public GamePlayBG bg;
	public PlayerLives lives;
	protected Sprites spriteMgr;
	private Texture2D[] character;
	private int curframe;
	private float curtime, timewanted, start, finish, _width, _height, speed;
	private int stand = 0, run1 = 1, run2 = 2, jump = 3, crouch = 4, die = 5;
	private int floorY = -85, xmin = -127, xmax = 127, platformY = -59, currentAnimation;
	private float startTime, duration = 2.5f;
	private bool falling, jumping, onplatform; 
	public bool smacked, bulldozed;
	private float runAnimation = 1;
	#endregion
	
	void Start () 
	{
		#region prepareeeeee
		currentAnimation = 0;
		rigidbody.velocity = Vector3.zero;
		transform.position = new Vector3(0, floorY, 0);
		
		smacked = false;
		bulldozed = false;
		falling = false;
		onplatform = false;
		speed = 20;
		startTime = -1;
		spriteMgr = new Sprites();
		character = new Texture2D[]
		{
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1),
			new Texture2D(1,1)
		};
		
		spriteMgr.GetSprite(spriteMgr.playerStand,  ref character[stand]);
		spriteMgr.GetSprite(spriteMgr.playerRun1,   ref character[run1]);
		spriteMgr.GetSprite(spriteMgr.playerRun2,   ref character[run2]);
		spriteMgr.GetSprite(spriteMgr.playerJump,   ref character[jump]);
		spriteMgr.GetSprite(spriteMgr.playerCrouch, ref character[crouch]);
		spriteMgr.GetSprite(spriteMgr.playerHurt,   ref character[die]);
		
		curframe = stand;
		
		UpdateMesh();
		#endregion
	}
	
	private bool PossiblePlatform()
	{
		return (bg.CanBeGrounded());
	}
	
	#region We all gotta die someday...
	public void KillType(string which)
	{
		if(which.Equals ("punch"))
		{
			smacked = true;
		}
		else
			bulldozed = true;
	}
	#endregion

	void Update () 
	{
		#region If dying
		if(smacked || bulldozed)
		{
			rigidbody.useGravity = false;
			currentAnimation++;
			curframe = die;
			UpdateMesh();
			
			if(smacked)
			{
				if(currentAnimation == 3)
					rigidbody.velocity = Vector3.up * speed/2;

				if(currentAnimation == 50) 
					rigidbody.velocity = Vector3.up * speed * speed + (Vector3.right * speed * speed * transform.localScale.x * -1);
				
				if(currentAnimation == 120)
				{
					smacked = false;
					Death ();
				}
			}
			
			if(bulldozed)
			{
				if(currentAnimation == 3)
					rigidbody.velocity = Vector3.right * transform.localScale.x * -1 * speed/2;

				if(currentAnimation == 50) 
					rigidbody.velocity = Vector3.right * speed * speed * transform.localScale.x * -1;
				
				if(currentAnimation == 120)
				{
					bulldozed = false;
					Death ();
				}
			}
			
			return;
		}
		#endregion
		runAnimation++;
		#region bounds
		switch((int)transform.localScale.x)
		{
			case -1:
				if(bg.CanScroll (1))
				{
					if(Mathf.Abs (transform.position.x) < 0.9f)
					transform.position = new Vector3(0, transform.position.y, 0);
				}
				break;
			case 1:
				if(bg.CanScroll (2))
				{
					if(Mathf.Abs (transform.position.x) < 0.9f)
					transform.position = new Vector3(0, transform.position.y, 0);
				}
				break;
		}
		
		#region Platforms
		if(PossiblePlatform() && !jumping)
		{
			if(Mathf.Abs (transform.position.y - platformY) < 1f)
			{
				transform.position = new Vector3(transform.position.x, platformY, 0);
				onplatform = true;
			}
		}
		else
		{
			onplatform = false;
			
			if(transform.position.y == platformY)
				falling = true;
		}
		#endregion
		
		if (transform.position.y < floorY) //:regular ground
		{
			transform.position = new Vector3 (transform.position.x, floorY, 0);
		}
		#endregion
		else
		{
			#region Gravity
			if(Grounded())
			{
				rigidbody.useGravity = false;
				falling = false;
				jumping = false;
			}
			else
				rigidbody.useGravity = true;
			#endregion
			
			#region bounds
			if(transform.position.x < xmin)
				transform.position = new Vector3(xmin, transform.position.y, 0);
			if(transform.position.x > xmax)
			{
				Application.LoadLevel("GameOver");
			}
			#endregion
			
			#region jumping
			if(Input.GetButton("Jump"))
			{
				onplatform = false;
					
				curframe = jump;
				UpdateMesh ();
				if((startTime == -1) && !falling)
				  startTime = Time.time;
				
				if(!falling && startTime != -1)
				{
					rigidbody.velocity = Vector3.up * speed + new Vector3(rigidbody.velocity.x, 0, 0);
					jumping = true;
				}
				
				//:duration = max jump duration
				if((Time.time - startTime) > duration)
				{
					startTime = -1;
					falling = true;
					jumping = false;
				}
			}//:Jumping
			
			if(Input.GetButtonUp ("Jump"))
			{
				jumping = false;
				falling = true;
			}
			
			if(!jumping && rigidbody.velocity.y > 0) //:fix occasional platform jump error
			{
				rigidbody.useGravity = true;
			}
			#endregion
			
			#region moving
			if(Input.GetAxis("Horizontal") != 0)
			{
				if(runAnimation >= 5 && !falling && !jumping)
				{
					runAnimation = 0;
					if(curframe == stand || curframe == run2)
					{
						curframe = run1;
						UpdateMesh();
					}
					else
					{
						curframe = run2;
						UpdateMesh();
					}
				}
				if(Input.GetAxis("Horizontal") > 0)
				{
					transform.localScale = (new Vector3(1, 1, 1));
					
					if(!bg.CanScroll(2) || !(transform.position.x == 0)) //:right
					{
					  rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed + 
					                                       new Vector3(0, rigidbody.velocity.y, 0);
					}
					else
					{
						bg.Scroll(Vector3.right * Input.GetAxis("Horizontal") * speed);
					}
					
					
				}
				else if(Input.GetAxis("Horizontal") < 0)
				{
					transform.localScale = (new Vector3(-1, 1, 1));
					if(!bg.CanScroll(1) || !(transform.position.x == 0)) //:left
					{
					  rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed + 
					                                       new Vector3(0, rigidbody.velocity.y, 0);
					}
					else
					{
						bg.Scroll(Vector3.right * Input.GetAxis("Horizontal") * speed);
					}
				}
			}//:Moving
			else
			{
				if(!falling && !jumping)
				{
					curframe = stand;
					UpdateMesh();
				}
				
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity, new Vector3(0, rigidbody.velocity.y, 0), 2);
				bg.Scroll (Vector3.zero);
			}//:stopping
			#endregion
		}
	}
	
	private bool Grounded()
	{
		return transform.position.y == floorY || onplatform;
	}
	
	private void Death()
	{
		lives.Death();
	}
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{
		_width  = character[curframe].width;
		_height = character[curframe].height;
		
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Diffuse"));
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		Vector3[] verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = character[curframe];
	}
}
