using UnityEngine;
using System.Collections;
using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class EndJug : MonoBehaviour 
{
	private Sprites spriteMgr;
	public GUIText score;
	private Vector3[] verts;
	private Texture2D TheJug;
	private int _width, _height;
	private float waitTime;
	
	// Use this for initialization
	void Start () 
	{
		waitTime = 0;
		spriteMgr = new Sprites();
		
		spriteMgr.GetSprite (spriteMgr.enemyAtk6, ref TheJug);
		
		_width = TheJug.width;
		_height = TheJug.height;
		UpdateMesh();
		
		//StreamReader sr;
		//string hiscore = "";
		
		//if(!Directory.Exists (Directory.GetCurrentDirectory() + "/Save/"))
		//	Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/Save/");
		
		
		//using(sr = new StreamReader(Directory.GetCurrentDirectory() + "/Save/iswearitsnot.png"))
		//{
		//	hiscore = sr.ReadLine();
		//	sr.Close();
		//}
		
		score.text = "";
	}
	
	void Update()
	{
		if(waitTime < 2f)
		waitTime += Time.deltaTime;
		
		if(waitTime > 2f)
			if(Input.anyKey)
				Application.LoadLevel("Start");
	}

	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Diffuse"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = TheJug;
	}
}
