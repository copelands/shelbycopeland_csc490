using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Platform : MonoBehaviour 
{
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D bg;
	private int _width, _height;
	
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();
		
		spriteMgr.GetSprite (spriteMgr.platform, ref bg);
		
		_width = bg.width;
		_height = bg.height;
		UpdateMesh();
	}
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Vertexlit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}
