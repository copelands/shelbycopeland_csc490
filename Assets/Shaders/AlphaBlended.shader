Shader "Custom/AlphaBlended" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	Category 
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull off Lighting off ZWrite off
		Blend SRCALPHA OneMinusSRCALPHA
		
		//:only for use in FIXED FUNCTION SHADERS
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		
		SubShader
		{
			Pass
			{
				SetTexture[_MainTex]
				{
					combine texture * primary
				}
			}//:end PASS
		}//:end SHADER
	} 
	FallBack "Diffuse"
}
