Shader "Custom/FixedTest" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1) 
	}
	//:darken
	SubShader
	{
		Tags{ "Queue"="Transparent" "RenderType"="Transparent" }
		Blend one one
		BlendOp Min
		cull off lighting off zwrite off
		
		bindchannels
		{
			bind "Color", color
			bind "Vertex", vertex
			bind "TexCoord", texcoord
		}
		
		Pass
		{
			SetTexture[_MainTex]
			{
				combine texture * primary
			}
			SetTexture[_MainTex]
			{
				constantcolor (1,1,1,1)
				combine previous lerp(previous) constant
			}
		}
	}
	
	//:fixed shader
	SubShader 
	{
		Lighting on
		
		Pass
		{
			Material
			{
				Diffuse[_Color]
			}
			SetTexture[_MainTex]
			{
				//constantColor[_Color]
				combine texture * primary
			}
		}
	} 
	FallBack "Diffuse"
}
