Shader "Custom/MultiplyLayer" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	Category 
	{
	//screen = Sr * one + Dst * oneminussrccolor
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull off Lighting off ZWrite off
		Blend ZERO SRCCOLOR
				
		//:only for use in FIXED FUNCTION SHADERS
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		
		SubShader
		{
			Pass
			{
				SetTexture[_MainTex]
				{
					combine texture * primary
				}
				
				SetTexture[_MainTex]
				{
					constantcolor (1,1,1,1)
					combine previous lerp(previous) constant
				}
			}//:end PASS
		}//:end SHADER
	} 
	FallBack "Diffuse"
}