Shader "Custom/SurfaceShader1" 
{
	Properties 
	{
		_MainTex 	("Base (RGB)", 2D) = "white" {}
		_Color 		("Blending Color", color) = (1,1,1,1)
		_Cutoff 	("Cutoff", Range(0,1)) = 0.5
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D 	_MainTex;
		float4 		_Color;
		float 		_Cutoff;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex);
			half4 cut = floor(c + _Cutoff);
			
			c = lerp(c, _Color, cut);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
