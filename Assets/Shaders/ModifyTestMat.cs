using UnityEngine;
using System.Collections;

public class ModifyTestMat : MonoBehaviour 
{
	private float delay;
	
	void Start () 
	{
		delay = 0f;
	}
	
	void Update () 
	{
		delay += Time.deltaTime;
		
		if(delay < 0.1f)
			return;
		
		delay = 0;
		Material mat = renderer.material;
		mat.SetColor("_Colour", new Color(Random.value, Random.value, Random.value, 1));
	}
}
