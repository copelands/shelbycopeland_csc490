Shader "Custom/TestShader" 
{
	Properties 
	{
		_Colour ("My Colour", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader 
	{
						//:Opaque or Transparent
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float4 _Colour;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c  = tex2D (_MainTex, IN.uv_MainTex);
			//c *= _Time.x;
			o.Albedo = c.rgb * _Colour;
			o.Alpha  = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}