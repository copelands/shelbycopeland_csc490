Shader "Custom/LinearBurn" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader 
	{
		Tags{"Queue"="Transparent" "RenderType"="Transparent"}
		Blend one one
		BlendOp RevSub
		cull off lighting off zwrite off
		
		bindchannels
		{
			bind "Color", color
			bind "Vertex", vertex
			bind "TexCoord", texcoord
		}
		
		Pass
		{
			SetTexture[_MainTex]
			{
				combine texture * primary
			}
			SetTexture[_MainTex]
			{
				constantColor(1,1,1,1)
				combine constant - previous
			}
			SetTexture[_MainTex]
			{
				constantcolor(0,0,0,0)
				combine previous lerp(texture) constant
			}
		}
	} 
	FallBack "Diffuse"
}
