Shader "Custom/Flowing"
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BumpTex ("Normal Map", 2D) = "white" {}
		_FlowMap ("Flow Map (RG)", 2D) = "white" {}
		_NoiseMap ("Noise Map (RGB)", 2D) = "white" {}
		_Offset ("Offset", Vector) = (0.5, 0.5, 0.5, 0.5)
		_CycleTime ("Cycle Time", range(0.1, 10)) = 1
		_HorizonColor ("Horizon Color", Color) = (1,1,1,1)
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _BumpTex;
		sampler2D _FlowMap;
		sampler2D _NoiseMap;
		float _CycleTime;
		float4 _HorizonColor;
		float4 _Color;

		struct Input 
		{
			float3 viewDir;
			float2 uv_MainTex;
			float2 uv_BumpTex;
			float2 uv_FlowMap;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 flowmap = tex2D(_FlowMap, IN.uv_FlowMap).rg - 0.5f;
			float flowStrength = length(flowmap);
			flowmap = flowmap * 2.0f;
			
			float halfCycleTime = _CycleTime * 0.5f;
			float noise = tex2D(_NoiseMap, IN.uv_FlowMap).r;
			float phase0 = fmod(_Time.x + halfCycleTime + noise, _CycleTime);
			float phase1 = fmod(_Time.x + noise, 1);
			float3 norm0 = UnpackNormal(tex2D(_BumpTex, IN.uv_BumpTex + flowmap * phase0));
			float3 norm1 = UnpackNormal(tex2D(_BumpTex, IN.uv_BumpTex + flowmap * phase1));
			
			float opacity = abs(phase0 - halfCycleTime) / halfCycleTime;
			float3 norm = lerp(norm0, norm1, opacity);
			float frensel = dot(IN.viewDir, norm);
		
		    // can have *_Color by tex2d
			half4 c = lerp(tex2D (_MainTex, IN.uv_MainTex) *_Color, _HorizonColor, frensel);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = normalize(norm * flowStrength);
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
