using System;

namespace AssemblyCSharp
{
	public class Vars
	{
		public const int LEFT  = -109;
		public const int RIGHT =  109;
		
		public int Bricks;
		
		
		private int paddleSize = 2;
		
		public int PaddleSize
		{
			get{ return paddleSize; }
			set{ int temp = value; paddleSize = temp/2; }
		}
	}
}

