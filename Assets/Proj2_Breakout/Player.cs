using System;
using System.Collections;
using System.IO;

using AssemblyCSharp;
using UnityEngine;

#pragma warning disable 0414

public class Player : MonoBehaviour 
{
	public GameObject ball;
	public TextMesh tm_lives, tm_score, tm_hiScore;
	private Vars v;
	private int lives, score, speed, hiScore;
	private bool started, canLeft, canRight;
	private string code;
	private Vector3 defaultBallPOS, defaultPOS;
	
	void Start() 
	{
		#region init
		LoadScore();
		
		v     = new Vars();
		speed = 25;
		code  = "";
		lives = 3;
		
		v.Bricks = 54;
		
		canLeft  = true;
		canRight = true;
			
		v.PaddleSize   = (int)this.renderer.bounds.size.x;
		defaultBallPOS = ball.transform.position; 
		defaultPOS = transform.position;
		#endregion
	}
	
	//:TODO:
	//:If all bricks are gone stop? -> Can't find requirement for it.
	
	
	void Update() 
	{
		if(code.Length > 20) code = "";
		
		#region speed up or slow down paddle movement
		if(Input.GetKey(KeyCode.F) && speed < 75)
			speed++;
		
		if(Input.GetKey(KeyCode.S) && speed > 1)
			speed--;
		
		#endregion
		
		#region codes
		if(Input.GetKeyDown (KeyCode.E))
			code += "e";
		
		if(Input.GetKeyDown (KeyCode.Y))
			code += "y";
		
		if(Input.GetKeyDown (KeyCode.H))
			code += "h";
		
		if(Input.GetKeyDown (KeyCode.R))
			code += "r";
		
		if(Input.GetKeyDown (KeyCode.A))
			code += "a";
		
		if(Input.GetKeyDown(KeyCode.D))
			code += "d";
		
		if(Input.GetKeyDown (KeyCode.S))
			code += "s";
		
		
		if(code.Length > 3 && code.Contains ("hard") && started && transform.localScale.x > 42)
		{
			code = "";
			transform.localScale = new Vector3(transform.localScale.x - 10, 
				                                 transform.localScale.y, 
				                                 transform.localScale.z);
			
			v.PaddleSize = (int)renderer.bounds.size.x;
		}
		
		if(code.Length > 3 && code.Contains ("easy") && started&& transform.localScale.x < 100)
		{
			code = "";
			transform.localScale = new Vector3(transform.localScale.x + 20, 
				                                 transform.localScale.y, 
				                                 transform.localScale.z);
			
			v.PaddleSize = (int)renderer.bounds.size.x;
		}
		#endregion
		
		#region Movement
		#region prevent off screen movement
			if(transform.position.x > Vars.RIGHT - v.PaddleSize)
		  {
				canRight = false;		
				transform.position += Vector3.left * speed * Time.deltaTime;			
				rigidbody.velocity = Vector3.zero;
		  }
		  if(transform.position.x < Vars.LEFT + v.PaddleSize)
		  {		
			  canLeft = false;
				transform.position += Vector3.right * speed * Time.deltaTime;
				rigidbody.velocity = Vector3.zero;
		  }
		
		  if(!started)
			  ball.transform.position = new Vector3(transform.position.x, defaultBallPOS.y, defaultBallPOS.z);
		
      #endregion
			
		if(canLeft && (Input.GetAxis("Horizontal") < 0 || Input.GetKey(KeyCode.A)))
		{
		  if(!started)
		 	  ball.rigidbody.velocity = speed * Vector3.left;
				
			rigidbody.velocity = speed * Vector3.left;
		 }
  	else if(canRight && (Input.GetAxis("Horizontal") > 0 || Input.GetKey (KeyCode.D)))
		{
			if(!started)
		  	ball.rigidbody.velocity = speed * Vector3.right;
			
		   rigidbody.velocity = speed * Vector3.right;
		}
		else
		{
			if(!started)
		  	ball.rigidbody.velocity = speed * Vector3.zero;
				
			rigidbody.velocity = Vector3.zero;
		}
			
		canLeft  = true;
		canRight = true;
		#endregion
		
		#region Ball
		
		if(!started)
			if(Input.GetKey(KeyCode.Space))
			{
				ball.rigidbody.velocity = (Vector3.up + Vector3.left) * (speed * 1.5f);
				started = true;
			}
		
		if(ball.transform.position.y < transform.position.y)
		{
			started = false;
			ball.transform.position = new Vector3(transform.position.x, defaultBallPOS.y, transform.position.z);	
			lives--;
			
			if(lives == 0)
			{
				SaveScore();
				
				tm_score.text = "0";
				score = 0;
				
				lives = 3;
				foreach(Brick b in FindSceneObjectsOfType(typeof(Brick)))
					b.Reset();
			}
			
			tm_lives.text = "Life Insurance Policies: " + lives;
		}
		#endregion
	}
	
	
	#region Save/Load

	//:Don't know if I want this.
	void OnCollisionEnter(Collision c)
	{			
		ball.rigidbody.velocity = (1.5f * speed * Vector3.up) + new Vector3(ball.rigidbody.velocity.x, 0, 0);
		
		v.Bricks = 0;
		foreach(Brick b in FindSceneObjectsOfType(typeof(Brick)))
		{
			if(b.transform.position.x > -200)
				v.Bricks++;
		}
		
		if(v.Bricks == 0)
		{
			foreach(Brick b in FindSceneObjectsOfType(typeof(Brick)))
					b.Reset();
		}
	}
	
	void LoadScore()
	{
		if(File.Exists ("Save/totallynotyoursavefile.png"))
		{
			StreamReader sr;
			
			using(sr = new StreamReader("Save/totallynotyoursavefile.png"))
			{
				try
				{
					hiScore         = Int32.Parse(sr.ReadLine());
					tm_hiScore.text = hiScore + "";
				}
				catch(Exception){/*y u edit my file*/}
				
				sr.Close();
			}
			return;
		}//: if file exists
		
		if(!Directory.Exists ("Save"))
		{
			Directory.CreateDirectory("Save");
		}
	}//: loadscore()
	
	
	void SaveScore()
	{
		hiScore = Int32.Parse(tm_hiScore.text);
	  score   = Int32.Parse(tm_score.text);
		
		if(score > hiScore)
		{
			tm_hiScore.text = score + "";
			
			StreamWriter sw;
			
			using(sw = new StreamWriter("Save/totallynotyoursavefile.png"))
			{
				sw.WriteLine(score);
				sw.Close();
			}
			
		}//: if score > hiscore
	}//: savescore()
  #endregion
}
