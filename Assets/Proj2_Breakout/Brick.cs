using UnityEngine;
using System.Collections;
using System;
using AssemblyCSharp;

public class Brick : MonoBehaviour 
{
	public GameObject ball;
	public TextMesh score;
	private bool hit;
	
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnCollisionEnter(Collision c)
	{
		transform.position += Vector3.left * 500;
		hit = true;
		
		score.text = (Int32.Parse(score.text) + 10) + "";
		
	}
	
	public void Reset()
	{
		if(hit)
		  transform.position += Vector3.right * 500;
		
		hit = false;
	}
}
